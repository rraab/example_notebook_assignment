#!/usr/bin/env python3

import os
import json
import click
import pytest
import requests

SERVER = 'localhost'
PORT = 12345
CONFIG = "config.json"
EXT = '.ipynb'

def main():

    # First ensure no pytest errors
    run_pytest()

    # Next, ensure we know student cruzid and password
    cruzid, password, assignment_number = get_cruzid_from_config()

    # check that cruzid and password are valid
    authenticate(cruzid, password)

    # Submit files to server
    upload_submission(cruzid, password, assignment_number)

def run_pytest():

    pytest_pipe = os.popen('pytest --color=yes')
    pytest_output = pytest_pipe.read()
    pytest_exit_code = pytest_pipe.close()

    if pytest_exit_code:
        print(pytest_output)
        print("There are failing tests.")
        if click.confirm("Are you sure you still want to submit?"):
            pass
        else:
            exit(1)
    else:
        print("Submission passes local tests. Nice. We'll double-check.")

def get_cruzid_from_config():

    # read the config file
    try:
        with open(CONFIG) as f:
            cfg = json.load(f)
    except FileNotFoundError as e:
        print(f"{CONFIG} could not be found. Writing file.")
        with open(CONFIG, "w") as f:
            json.dump({"cruzid": "sslug", "password": "1234567890", "assignment_number": "0"}, f)
        print(f"Please enter your CRUZ_ID, password, and assignment number in {CONFIG}")
        exit(1)
    except json.decoder.JSONDecodeError:
        print(f"{CONFIG} appears to be malformed json.")
        print("Fix or delete the file and try again.")
        exit(1)

    # extract cruzid and password
    try:
        cruzid = cfg['cruzid']
        password = cfg['password']
        assignment_number = cfg['assignment_number']
        print(f"Your CRUZ_ID in {CONFIG} is '{cruzid}'.")
    except KeyError as e:
        print(f'{CONFIG} is valid json, but "cruzid", "password", or "assignment_number" not found.')
        print("Fix or delete the file and try again.")
        exit(1)

    return cruzid, password, assignment_number

def authenticate(cruzid, password):

    print(f"Checking '{cruzid}' against student roster... ", end='')

    r = requests.post(
        f'http://{SERVER}:{PORT}',
        auth=(cruzid, password)
    )

    if r.status_code == 200:
        response = r.json()
        print('Authenticated.')
        print(json.dumps(response, indent=2))
    elif r.status_code == 401:
        print('Invalid cruzid or password.')
        exit(1)
    else:
        print('Failed.')
        print(f'\nUnspecified Error: status code {r.status_code}.')
        exit(1)

def upload_submission(cruzid, password, assignment_number):

    # Find notebook:
    # - list of notebook file names in the current directory
    l = list(x for x in os.listdir(os.getcwd()) if x.endswith(EXT))
    # - there can only be one
    if len(l) != 1:
        print('Found mulitple notebooks in this directory. Expected 1.')
        exit(1)
    # - get that one
    nb = l[0]

    # submit notebook file as json to minimize potentional abuse
    with open(nb, 'r') as f:

        try:
            payload=json.load(f)
        except Exception as e:
            print(e.msg)
            print(f'{nb} does not have a valid JSON format.')
            exit(1)

        print(f'Submitting {nb}...')

        r = requests.post(
            f'http://{SERVER}:{PORT}/autograde',
            auth=(cruzid, password),
            json={
                'assignment_number': assignment_number,
                'notebook': payload
            }
        )

    if r.status_code == 401:
        print('Invalid cruzid or password.')
        exit(1)
    elif r.status_code != 200:
        print(f'\nUnspecified Server Error: status code {r.status_code}.')
        exit(1)

    print('\n' + '#' * 80)
    print('# Results\n')

    response = r.json()
    if 'Error' in response:
        print('Oh no! There\'s a problem with our script or server!')
        print(response['Error'])

    else:
        print(json.dumps(response, indent=2))

if __name__ == "__main__":
    main()
