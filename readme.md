# Example Assignment 

This set of files should contain:

```
├── assignment.ipynb       # This is your homework assignment.
├── test_assignment.py     # Tests run on your assignment are defined here.
├── config.json            # Your cruzid and password must be written here.
├── _ipynbimport.py        # Allows the tests to run.
└── submit.py              # Run this script to submit your assignment.
```

1. Edit `config.json` with your CRUZ_ID and PASSWORD for this assignment.
2. Work on the assignment notebook.
3. Test that solution passes local tests by running `pytest`.
5. Submit your assignment by running `python3 submit.py`.



